require(['dojo/query', 'dojo/dom-geometry' , 'dojo/dom-style', 'dojo/on', 'dojo/dom-class', 'dojo/_base/fx', 'dojo/dom-attr', 'dojo/NodeList-dom', 'dojo/NodeList-traverse','app/NodeList-Interator', 'dojo/domReady!'],
  function(query, domGeo, style, on, domClass, fx, attr){
  
  var getWidth = 0;
  var widthBase = domGeo.getContentBox(query('.content_slider_navegacao')[0]).w;
  var slider = query('.content_slider_navegacao ul');
  var itens  = slider.query('> li');
  var widthSlider = 0;
  var animate = false;

  var lastWidth = 0;
  var maxLeft = 0;

  itens.forEach(function(v){
    lastWidth = domGeo.getMarginBox(v).w;;
    widthSlider += lastWidth;
  });

  maxLeft = widthSlider-widthBase;
  getWidth = maxLeft+lastWidth;

  style.set(slider[0], 'width', widthSlider+'px');

  if(!domClass.contains( query('html')[0], 'csstransitions')){
    animate = true;
  }

  var animateSlider = function(left){

    if(left > maxLeft){
      left = maxLeft;
    }

    left = left * -1;

    if(animate){
      fx.animateProperty({
        node: slider[0],
        properties: {
          marginLeft: left
        }
      }).play();
    } else {
      style.set(slider[0], 'marginLeft', left + 'px');
    }
  }

  var goNext = function(){
    var position = itens.getNext();
    var left = 0;
    var size = position;

    while( size ){
      left += domGeo.getMarginBox(itens[size]).w;
      size--;
    }

    if(left > getWidth){
      itens.goTo(0);
      left = 0;
    } else {
      itens.goTo(position);
    }

    animateSlider(left);
  };

  var goPrev = function(){
    var position = itens.getPrev();
    var left = 0;
    var size = position;

    while( size ){
      left += domGeo.getMarginBox(itens[size]).w;
      size--;
    }

    itens.goTo(position);
    
    if(left > getWidth){
      goPrev();
      return;
    }

    animateSlider(left);
  };

  query('.next_navegacao').on('click', function(e){
    e.preventDefault();

    goNext();
  });

  query('.prev_navegacao').on('click', function(e){
    e.preventDefault();

    goPrev();
  });


  // grafico barra
  

  var graficos = query('.graficos span');
  
  graficos.forEach(function(item){

    var height = parseInt(attr.get(item, 'data-height'), 10);

    if(animate){

      fx.animateProperty({
        node: item,
        duration: 1000,
        properties: {
          height: height
        }
      }).play();

    } else {
      style.set(item, 'height', height+'px');
    }

  });
  
});