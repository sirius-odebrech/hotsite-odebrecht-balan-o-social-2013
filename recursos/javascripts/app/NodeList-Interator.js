define(['dojo/_base/lang',  'dojo/NodeList'],
  function(lang, NodeList){
    var currentNode = 0;
    var goToCallback = function(){};
    
    lang.extend(NodeList, {
      getCurrent: function(){
        return currentNode;
      },
      getNext : function(){
        return currentNode+1 === this.length ? 0 : currentNode+1;
      },

      getPrev : function(){
        return currentNode ? currentNode-1 : this.length - 1;
      },

      goTo : function(index){

        if(index > -1 && index < this.length){
          currentNode = index;
        }

        goToCallback.apply(this, [this.getPrev(), currentNode]);

        return this;
      },

      goNext : function(){
        var i = this.getNext();
        this.goTo(i);
        return i;
      },

      goPrev : function(){
        var i = this.getPrev();
        this.goTo(i);
        return i;
      },

      goToCallback : function(callback){
        if(typeof callback === 'function'){
          goToCallback = callback;
        }

        return this;
      }
    });

    return NodeList;
  }
);